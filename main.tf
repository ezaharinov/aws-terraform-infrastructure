locals {
  user_data = <<EOF
#!/bin/bash
echo "Hello Terraform!"
EOF
}
################ VPC #################
resource "aws_vpc" "main" {
  cidr_block       = var.main_vpc_cidr
  instance_tenancy = "default"
  enable_dns_support = true
  enable_dns_hostnames = true

}
 ################# Subnets #############

resource "aws_subnet" "subnet1public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.19.0.0/24"
  availability_zone = var.availability_zone1

  tags = {
    Name = "public-subnet-jump"
  }
}
resource "aws_subnet" "subnet2private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.19.1.0/24"
  availability_zone = var.availability_zone1

  tags = {
    Name = "private-subnet-1"
  }
}
resource "aws_subnet" "subnet3private" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "172.19.2.0/24"
  availability_zone = var.availability_zone2

  tags = {
    Name = "private-subnet-2"
  }
}
######## IGW ###############
resource "aws_internet_gateway" "main-igw" {
  vpc_id = aws_vpc.main.id
}
resource "aws_eip" "nat" {
}

resource "aws_nat_gateway" "main-natgw" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.subnet1public.id

  tags = {
    Name = "main-nat"
  }
}

############# Route Tables ##########
resource "aws_route_table" "main-public-rt" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-igw.id
  }

  tags = {
    Name = "main-public-rt"
  }
}
resource "aws_route_table" "main-private-rt" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.main-natgw.id
  }

  tags = {
    Name = "main-private-rt"
  }
}

######### PUBLIC Subnet assiosation with rotute table    ######
resource "aws_route_table_association" "public-assoc-1" {
  subnet_id      = aws_subnet.subnet1public.id
  route_table_id = aws_route_table.main-public-rt.id
}

########## PRIVATE Subnets assiosation with rotute table ######

resource "aws_route_table_association" "private-assoc-1" {
  subnet_id      = aws_subnet.subnet2private.id
  route_table_id = aws_route_table.main-private-rt.id
}
resource "aws_route_table_association" "private-assoc-2" {
  subnet_id      = aws_subnet.subnet3private.id
  route_table_id = aws_route_table.main-private-rt.id
}

##Public SEC Group JUMPHOST##

resource "aws_security_group" "ssh-allowed-sg-jump" {
  vpc_id = aws_vpc.main.id
  name = "bastion-host-jump"
  description = "allow SSH access from corporate IP"
}

resource "aws_security_group_rule" "ssh" {
  protocol          = "TCP"
  from_port         = 22
  to_port           = 22
  type              = "ingress"
  cidr_blocks       = ["94.156.253.114/32"]
  security_group_id = aws_security_group.ssh-allowed-sg-jump.id
}

resource "aws_security_group_rule" "internet" {
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh-allowed-sg-jump.id
}

resource "aws_security_group_rule" "intranet" {
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["172.19.0.0/24"]
  security_group_id = aws_security_group.ssh-allowed-sg-jump.id
}
  ###PRIVATE SEC GROUP####

resource "aws_security_group" "ssh-allow-private" {
  vpc_id = aws_vpc.main.id
  name = "private-sg"
  egress {
    from_port = 0
    protocol = -1
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["172.19.0.0/32"]

  }
}
##EC2###
data "aws_ami" "amazon_linux" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "my_ec2_instance" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = "t2.nano"
  subnet_id = aws_subnet.subnet1public.id
  vpc_security_group_ids = [aws_security_group.ssh-allowed-sg-jump.id]
  associate_public_ip_address = true
  key_name = var.key_pair_name
  tags = {
    Name = "Bastion-ec2"
  }
}
resource "aws_instance" "my_ec2_instance1" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = "t2.nano"
  subnet_id = aws_subnet.subnet2private.id
  vpc_security_group_ids = [aws_security_group.ssh-allow-private.id]
  tags = {
    Name = "Private-EC2-1"
  }
}
resource "aws_instance" "my_ec2_instance2" {
  ami = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.subnet3private.id

  vpc_security_group_ids = [aws_security_group.ssh-allow-private.id]
  tags = {
    Name = "Private-EC2-2"
  }
}