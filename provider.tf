provider "aws" {
  region = "us-west-2"
  version = "~> 2.8"
  shared_credentials_file = "/Users/emilian.zaharinov/.aws/credentials"
  profile = "technofy"
}