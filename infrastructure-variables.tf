variable "main_vpc_cidr" {
    description = "CIDR of the VPC"
    default = "172.19.0.0/16"
}
variable "availability_zones" {
  type        = list(string)
  description = "List of Availability Zones"
}
variable "Avalability_zone_list" {
    type = list(string)
    description = "List of Availability zones (e.g. `['us-east-1a', 'us-east-1b', 'us-east-1c']`)"
}
variable "availability_zone1" {
    description = "Avaialbility Zones1"
    default = "us-west-2a"
}

variable "availability_zone2" {
    description = "Avaialbility Zones2"
    default = "us-west-2b"
}

variable "key_pair_name" {
    default = "emo-key-oregon"
}